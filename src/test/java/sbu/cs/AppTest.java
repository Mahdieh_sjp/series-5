package sbu.cs;

import org.junit.jupiter.api.Test;
import sbu.cs.Functions.*;

import static org.junit.jupiter.api.Assertions.*;

class AppTest {

    @Test
    void main() {
        int n = 8;
        int[][] arr = {
                {2, 5, 5, 4, 2, 1, 5, 5},
                {2, 1, 2, 4, 4, 1, 5, 4},
                {4, 4, 1, 1, 1, 5, 1, 4},
                {4, 1, 4, 4, 1, 4, 5, 1},
                {1, 1, 1, 5, 1, 4, 4, 5},
                {4, 4, 5, 4, 5, 1, 5, 5},
                {1, 4, 4, 4, 1, 1, 1, 4},
                {4, 5, 4, 5, 5, 1, 4, 4}
        };
        String input = "qmiqwnhwnrckeirepjgv";
        assertEquals(new App().main(n, arr, input), "vemehewewjijejzjznenancncryrgrlrljjjpjljldedvdtdtmomimumu" +
                "sdsssosodldcdzdzmdmhmvmzizikizizxzxhxzxzpzptpzpzvzvdvzvzrzrirzrzizimizizvzvkvzvzkzktkzkzqzqwqzqztzto" +
                "tzt");

    }

    @Test
    void blackFunction1(){
        assertEquals(new Function1().func("abcde"), "edcba");
    }

    @Test
    void blackFunction2() {
        assertEquals(new Function2().func("abcde"), "aabbccddee");
    }

    @Test
    void blackFunction3() {
        assertEquals(new Function3().func("abcde"), "abcdeabcde");
    }

    @Test
    void blackFunction4() {
        assertEquals(new Function4().func("abcde"), "eabcd");
    }

    @Test
    void blackFunction5() {
        assertEquals(new Function5().func("abcde"), "zyxwv");
    }

    @Test
    void whiteFunction1(){
        assertEquals(new Function1().func("abcde", "vwx"), "avbwcxde");
    }

    @Test
    void whiteFunction2(){
        assertEquals(new Function2().func("abcde", "vwxyz"), "abcdezyxwv");
    }

    @Test
    void whiteFunction3(){
        assertEquals(new Function3().func("abcde", "vwxy"), "aybxcwdve");
    }

    @Test
    void whiteFunction4(){
        assertEquals(new Function4().func("abcd", "vwxyz"), "abcd");
        assertEquals(new Function4().func("abcde", "vwxyz"), "vwxyz");
    }

    @Test
    void whiteFunction5(){
        assertEquals(new Function5().func("abcd", "vwxyz"), "vxzbz");
    }

}