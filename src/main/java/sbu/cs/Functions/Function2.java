package sbu.cs.Functions;

import sbu.cs.BlackFunction;
import sbu.cs.WhiteFunction;

public class Function2 implements BlackFunction, WhiteFunction {
    @Override
    public String func(String str) {
        StringBuilder str2 = new StringBuilder();
        for(char c: str.toCharArray()){
            str2.append(c).append(c);
        }
        return str2.toString();
    }

    @Override
    public String func(String str1, String str2) {
        return str1
                + new StringBuilder().append(str2).reverse().toString();
    }
}

