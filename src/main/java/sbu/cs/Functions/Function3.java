package sbu.cs.Functions;

import sbu.cs.BlackFunction;
import sbu.cs.WhiteFunction;

public class Function3 implements BlackFunction, WhiteFunction {
    @Override
    public String func(String str) {
        return str + str;
    }

    @Override
    public String func(String str1, String str2) {
        StringBuilder str3 = new StringBuilder();
        str2 = new StringBuilder().append(str2).reverse().toString();

        int i = 0;
        for(; i < str1.length() && i < str2.length(); i++){
            str3.append(str1.charAt(i)).append(str2.charAt(i));
        }
        str3.append(str1.substring(i)).append(str2.substring(i));

        return str3.toString();
    }
}