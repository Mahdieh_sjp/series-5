package sbu.cs.Functions;

import sbu.cs.BlackFunction;
import sbu.cs.WhiteFunction;

public class Function1 implements BlackFunction, WhiteFunction {
    @Override
    public String func(String str) {
        return new StringBuilder().append(str).reverse().toString();
    }

    @Override
    public String func(String str1, String str2) {
        StringBuilder str3 = new StringBuilder();
        int i = 0;
        for(; i < str1.length() && i < str2.length(); i++){
            str3.append(str1.charAt(i)).append(str2.charAt(i));
        }
        str3.append(str1.substring(i)).append(str2.substring(i));

        return str3.toString();
    }
}