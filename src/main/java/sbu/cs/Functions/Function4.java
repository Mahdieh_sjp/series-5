package sbu.cs.Functions;

import sbu.cs.BlackFunction;
import sbu.cs.WhiteFunction;

public class Function4 implements BlackFunction, WhiteFunction {
    @Override
    public String func(String str) {
        return str.charAt(str.length() - 1)
                + str.substring(0, str.length() - 1);
    }

    @Override
    public String func(String str1, String str2) {
        if (str1.length() % 2 == 0){
            return str1;
        }
        return str2;
    }
}
