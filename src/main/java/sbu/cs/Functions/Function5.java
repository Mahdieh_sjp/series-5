package sbu.cs.Functions;

import sbu.cs.BlackFunction;
import sbu.cs.WhiteFunction;

public class Function5 implements BlackFunction, WhiteFunction {
    @Override
    public String func(String str) {
        StringBuilder str2 = new StringBuilder();

        for(char c: str.toCharArray()){
            str2.append((char)(Math.abs('z' - c) + 'a'));
        }
        return str2.toString();
    }

    @Override
    public String func(String str1, String str2) {
        StringBuilder str3 = new StringBuilder();

        int i = 0;
        for(; i < str1.length() && i < str2.length(); i++){
            str3.append((char)((str1.charAt(i) + str2.charAt(i) - 2*'a') % 26 + 'a'));
        }
        str3.append(str1.substring(i)).append(str2.substring(i));

        return str3.toString();
    }
}

