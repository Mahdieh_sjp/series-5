package sbu.cs;

public interface BAndWFunction {
    public String func(String str);
    public String func(String str1, String str2);
}
