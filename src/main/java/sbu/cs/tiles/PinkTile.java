package sbu.cs.tiles;

import sbu.cs.*;

public class PinkTile {

    public static StringBox[][] set(BAndWFunction[] functions, StringBox[][] strArr, int funcNum, int j, int i){
        if(i == MagicSquare.squareSize - 1) {
            strArr[j][i].setStr1(functions[funcNum].func(strArr[j][i - 1].getStr1()
                    , strArr[j - 1][i].getStr1()));
        }else {
            strArr[j][i].setStr1(functions[funcNum].func(strArr[j][i - 1].getStr1()
                    , strArr[j - 1][i].getStr2()));
        }
        return strArr;
    }
}
