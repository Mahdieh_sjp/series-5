package sbu.cs.tiles;

import sbu.cs.BAndWFunction;
import sbu.cs.StringBox;

public class BlueTile{

    public static StringBox[][] set(BAndWFunction[] functions, StringBox[][] strBox, int funcNum, int j, int i) {

        strBox[j][i].setStr1(functions[funcNum].func(strBox[j][i - 1].getStr1()));
        if (j == 1) {
            strBox[j][i].setStr2(functions[funcNum].func(strBox[j - 1][i].getStr1()));
        } else {
            strBox[j][i].setStr2(functions[funcNum].func(strBox[j - 1][i].getStr2()));
        }
        return strBox;
    }
}
