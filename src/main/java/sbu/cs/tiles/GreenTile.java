package sbu.cs.tiles;

import sbu.cs.BAndWFunction;
import sbu.cs.StringBox;

public class GreenTile {
    public static StringBox[][] set(BAndWFunction[] functions, StringBox[][] strBox, String input, int funcNum, int j, int i){

        if (i == 0 && j == 0){
            strBox[j][i].setStr1(functions[funcNum].func(input));
        } else{
            if (i == 0){
                strBox[j][i].setStr1(functions[funcNum].func(strBox[j - 1][i].getStr1()));
            } else{
                strBox[j][i].setStr1(functions[funcNum].func(strBox[j][i - 1].getStr1()));
            }
        }
        return strBox;
    }
}
