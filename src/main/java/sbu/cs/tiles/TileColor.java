package sbu.cs.tiles;

public class TileColor {

    public static boolean isAGreenTile(int n, int j, int i){
        if((i == 0 && j < n - 1) || (j == 0 && i < n - 1)){
            return true;
        }
        return false;
    }

    public static boolean isABlueTile(int n, int j, int i){
        if((i > 0 && i < n - 1) && (j > 0 && j < n - 1)){
            return true;
        }
        return false;
    }

    public static boolean isAYellowTile(int n, int j, int i){
        if((i == 0 && j == n - 1) || (j == 0 && i == n - 1)){
            return true;
        }
        return false;
    }

    public static boolean isAPinkTile(int n, int j, int i){
        if((i > 0 && j == n - 1) || (j > 0 && i == n - 1)){
            return true;
        }
        return false;
    }

}
