package sbu.cs;

import sbu.cs.Functions.*;
import sbu.cs.tiles.*;


public class MagicSquare {

    public static int squareSize;

    public static String getSquareOutput(int n, int[][] arr, String input){

        StringBox[][] strBox = new StringBox[n][n];
        BAndWFunction[] functions = initFunctions();

        setSquareSize(arr.length);


        for (int j = 0; j < n; j++) {
            for(int i = 0; i < n; i++){

                strBox[j][i] = new StringBox();

                if(TileColor.isAGreenTile(n, j, i)) {
                    strBox = GreenTile.set(functions, strBox, input, arr[j][i], j, i);
                } else if(TileColor.isAYellowTile(n, j, i)){
                    strBox = YellowTile.set(functions, strBox, arr[j][i], j, i);
                } else if(TileColor.isABlueTile(n, j, i)){
                    strBox = BlueTile.set(functions, strBox, arr[j][i], j, i);
                } else if(TileColor.isAPinkTile(n, j, i)){
                    strBox = PinkTile.set(functions, strBox, arr[j][i], j, i);
                }
            }
        }
        return strBox[squareSize - 1][squareSize - 1].getStr1();
    }

    public static void setSquareSize(int n){
        squareSize = n;
    }

    public static BAndWFunction[] initFunctions(){
        return new BAndWFunction[]{
                null,
                new Function1(),
                new Function2(),
                new Function3(),
                new Function4(),
                new Function5()
        };
    }
}
